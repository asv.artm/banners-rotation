package configer

import (
	"context"
	"log"

	"github.com/heetch/confita/backend/env"

	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
)

func ReadConfig(path string) *ConfigAPI {
	loader := confita.NewLoader(
		file.NewBackend(path),
		env.NewBackend(),
	)
	cfg := ConfigAPI{}
	err := loader.Load(context.Background(), &cfg)
	if err != nil {
		log.Fatalf("read config error: %v", err)
	}

	return &cfg
}
