package configer

type ConfigAPI struct {
	GrpcHost           string `config:"grpc_host,required"`
	GrpcPort           int    `config:"grpc_port,required"`
	PostgresDsn        string `config:"postgres_dsn,required"`
	RabbitURL          string `config:"rabbit_url,required"`
	RabbitQueue        string `config:"rabbit_queue,required"`
	RabbitExchange     string `config:"rabbit_exchange,required"`
	RabbitExchangeType string `config:"rabbit_exchange_type,required"`
	LogFile            string `config:"log_file,required"`
	LogLevel           string `config:"log_level,required"`
}
