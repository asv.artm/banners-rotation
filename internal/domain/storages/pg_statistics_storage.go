package storages

import (
	"context"

	"github.com/jmoiron/sqlx"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/entities"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/interfaces"
)

type PgStatisticsStorage struct {
	db *sqlx.DB
}

func NewPgStatisticsStorage(db *sqlx.DB) (interfaces.StatisticsStorage, error) {
	err := db.Ping()
	if err != nil {
		return nil, err
	}

	return &PgStatisticsStorage{db}, nil
}

func (pc *PgStatisticsStorage) SummaryBySlotAndGroup(ctx context.Context, slotID int64, groupID int64) ([]entities.StatisticsSummary, error) {
	query := `
		SELECT r.banner_id, $1 AS slot_id, $2 AS group_id,
			COALESCE(sb.buyouts, 0) as buyouts, 
			COALESCE(sc.clicks, 0) AS clicks
		FROM rotation r 
		LEFT JOIN (
			SELECT banner_id, group_id, COUNT(*) as clicks
			FROM "statistics" s 
			WHERE slot_id = $1 and group_id = $2 AND type_id = 1
			GROUP BY type_id, banner_id, slot_id, group_id
		) AS sc ON sc.banner_id = r.banner_id
		LEFT JOIN (
			SELECT banner_id, COUNT(*) as buyouts
			FROM "statistics" s 
			WHERE slot_id = $1 and group_id = $2 AND type_id = 2
			GROUP BY type_id, banner_id, slot_id, group_id
		) AS sb ON sb.banner_id = r.banner_id
		WHERE slot_id = $1
	`

	var stats []entities.StatisticsSummary
	err := pc.db.SelectContext(ctx, &stats, query, slotID, groupID)
	if err != nil {
		return nil, err
	}

	return stats, nil
}

func (pc *PgStatisticsStorage) Add(ctx context.Context, item *entities.Statistics) error {
	query := `
		INSERT INTO statistics(type_id, banner_id, slot_id, group_id, date_time)
		VALUES (:type_id, :banner_id, :slot_id, :group_id, :date_time)
	`
	_, err := pc.db.NamedExecContext(ctx, query, item)
	if err != nil {
		return err
	}

	return nil
}
