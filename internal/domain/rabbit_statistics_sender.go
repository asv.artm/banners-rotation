package domain

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/interfaces"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/entities"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/enums"
	rabbitmq "gitlab.com/asv.artm/banners-rotation/pkg/rabbitmq-producer"
)

type RabbitStatisticsSender struct {
	producer *rabbitmq.Producer
}

func NewStatisticsSender(producer *rabbitmq.Producer) interfaces.StatisticsSender {
	return &RabbitStatisticsSender{producer: producer}
}

func (s *RabbitStatisticsSender) Send(statType enums.StatisticType, bannerID int64, slotID int64, groupID int64) error {
	item := entities.Statistics{
		TypeID:   int64(statType),
		BannerID: bannerID,
		SlotID:   slotID,
		GroupID:  groupID,
		DateTime: time.Now(),
	}

	body, err := json.Marshal(item)
	if err != nil {
		return fmt.Errorf("error on marshal statistic: %v", err)
	}

	err = s.producer.Publish(body)
	log.Printf("send statistic to analytics system: %s\n", body)
	if err != nil {
		return fmt.Errorf("failed to publish a message: %v", err)
	}

	return nil
}
