package domain

import (
	"context"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/entities"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/enums"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/interfaces"
	"gitlab.com/asv.artm/banners-rotation/pkg/algorithm"
	algentities "gitlab.com/asv.artm/banners-rotation/pkg/algorithm/entities"
	alginterfaces "gitlab.com/asv.artm/banners-rotation/pkg/algorithm/interfaces"
)

type BannerRotator struct {
	algorithm        alginterfaces.RotationAlgorithm
	bannerStorage    interfaces.BannerStorage
	slotStorage      interfaces.SlotStorage
	groupStorage     interfaces.GroupStorage
	rotationStorage  interfaces.RotationStorage
	statisticStorage interfaces.StatisticsStorage
	logger           *logrus.Logger
}

func NewBannerRotator(
	bannerStorage interfaces.BannerStorage,
	slotStorage interfaces.SlotStorage,
	groupStorage interfaces.GroupStorage,
	rotationStorage interfaces.RotationStorage,
	statisticStorage interfaces.StatisticsStorage,
	logger *logrus.Logger,
) (interfaces.Rotator, error) {
	algorithm, err := algorithm.NewMultiarmedBandit(logger)
	if err != nil {
		return nil, fmt.Errorf("error on init rotation algorithm")
	}

	rotator := &BannerRotator{
		bannerStorage:    bannerStorage,
		slotStorage:      slotStorage,
		groupStorage:     groupStorage,
		rotationStorage:  rotationStorage,
		statisticStorage: statisticStorage,
		algorithm:        algorithm,
		logger:           logger,
	}
	return rotator, nil
}

func (r *BannerRotator) Add(ctx context.Context, bannerID int64, slotID int64) error {
	banner, err := r.bannerStorage.Get(ctx, bannerID)
	if err != nil {
		return fmt.Errorf("error on get banner %d from database: %v", bannerID, err)
	}
	if banner == nil {
		return fmt.Errorf("banner %d not found in database", bannerID)
	}

	slot, err := r.slotStorage.Get(ctx, slotID)
	if err != nil {
		return fmt.Errorf("error on get slot %d from database: %v", slotID, err)
	}
	if slot == nil {
		return fmt.Errorf("slot %d not found in database", slotID)
	}

	found, err := r.rotationStorage.Get(ctx, bannerID, slotID)
	if err != nil {
		return fmt.Errorf("error on get rotation item for bannner %d and slot %d: %v", bannerID, slotID, err)
	}
	if found != nil {
		return fmt.Errorf("banner %d already exists in rotation for slot %d", bannerID, slotID)
	}

	item := &entities.Rotation{
		BannerID:  bannerID,
		SlotID:    slotID,
		StartedAt: time.Now(),
	}
	err = r.rotationStorage.Add(ctx, item)
	if err != nil {
		return fmt.Errorf("error on add banner %d in rotation for slot %d: %v", bannerID, slotID, err)
	}

	return nil
}

func (r *BannerRotator) Delete(ctx context.Context, bannerID int64, slotID int64) error {
	event, err := r.rotationStorage.Get(ctx, bannerID, slotID)
	if err != nil {
		return fmt.Errorf("error on get rotation for banner %d and slot %d: %v", bannerID, slotID, err)
	}
	if event == nil {
		return fmt.Errorf("banner %d not found in rotation for slot %d", bannerID, slotID)
	}

	err = r.rotationStorage.Delete(ctx, bannerID, slotID)
	if err != nil {
		return fmt.Errorf("error on delete banner %d from rotation for slot %d: %v", bannerID, slotID, err)
	}

	return nil
}

func (r *BannerRotator) Get(ctx context.Context, slotID int64, groupID int64) (int64, error) {
	statistics, err := r.statisticStorage.SummaryBySlotAndGroup(ctx, slotID, groupID)
	if err != nil {
		return 0, err
	}

	data := make([]algentities.AlgorithmData, 0)
	for _, stat := range statistics {
		item := algentities.AlgorithmData{
			HandleID:  stat.BannerID,
			Count:     stat.Buyouts,
			AvgIncome: stat.Clicks,
		}
		data = append(data, item)
	}

	return r.algorithm.GetHandle(data)
}

func (r *BannerRotator) Click(ctx context.Context, bannerID int64, slotID int64, groupID int64) error {
	err := r.addStatistic(ctx, enums.Click, bannerID, slotID, groupID)
	if err != nil {
		return fmt.Errorf("error on save statistic by click: %v", err)
	}

	return nil
}

func (r *BannerRotator) Buyout(ctx context.Context, bannerID int64, slotID int64, groupID int64) error {
	err := r.addStatistic(ctx, enums.Buyout, bannerID, slotID, groupID)
	if err != nil {
		return fmt.Errorf("error on save statistic by buyout: %v", err)
	}

	return nil
}

func (r *BannerRotator) addStatistic(ctx context.Context, statType enums.StatisticType, bannerID int64, slotID int64, groupID int64) error {
	item := &entities.Statistics{
		TypeID:   int64(statType),
		BannerID: bannerID,
		SlotID:   slotID,
		GroupID:  groupID,
		DateTime: time.Now(),
	}

	err := r.statisticStorage.Add(ctx, item)
	if err != nil {
		return fmt.Errorf("error on save statistic, banner %d slot %d group %d: %v", bannerID, slotID, groupID, err)
	}

	return nil
}
