package entities

import "time"

type Statistics struct {
	TypeID   int64     `db:"type_id"`
	BannerID int64     `db:"banner_id"`
	SlotID   int64     `db:"slot_id"`
	GroupID  int64     `db:"group_id"`
	DateTime time.Time `db:"date_time"`
}
