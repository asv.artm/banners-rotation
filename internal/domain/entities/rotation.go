package entities

import "time"

type Rotation struct {
	BannerID  int64     `db:"banner_id"`
	SlotID    int64     `db:"slot_id"`
	StartedAt time.Time `db:"started_at"`
}
