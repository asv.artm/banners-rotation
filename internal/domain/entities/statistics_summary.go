package entities

type StatisticsSummary struct {
	BannerID int64 `db:"banner_id"`
	SlotID   int64 `db:"slot_id"`
	GroupID  int64 `db:"group_id"`
	Buyouts  int64 `db:"buyouts"`
	Clicks   int64 `db:"clicks"`
}
