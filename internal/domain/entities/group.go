package entities

type Group struct {
	ID    int64
	Title string
}
