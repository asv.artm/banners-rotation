package entities

type Banner struct {
	ID    int64
	Title string
}
