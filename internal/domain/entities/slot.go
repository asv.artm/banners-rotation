package entities

type Slot struct {
	ID    int64
	Title string
}
