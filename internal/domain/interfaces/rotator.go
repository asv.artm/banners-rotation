package interfaces

import (
	"context"
)

type Rotator interface {
	Add(ctx context.Context, bannerID int64, slotID int64) error
	Delete(ctx context.Context, bannerID int64, slotID int64) error
	Click(ctx context.Context, bannerID int64, slotID int64, groupID int64) error
	Buyout(ctx context.Context, bannerID int64, slotID int64, groupID int64) error
	Get(ctx context.Context, slotID int64, groupID int64) (int64, error)
}
