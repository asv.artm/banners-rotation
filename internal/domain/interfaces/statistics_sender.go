package interfaces

import "gitlab.com/asv.artm/banners-rotation/internal/domain/enums"

type StatisticsSender interface {
	Send(statType enums.StatisticType, bannerID int64, slotID int64, groupID int64) error
}
