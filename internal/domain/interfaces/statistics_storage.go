package interfaces

import (
	"context"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/entities"
)

type StatisticsStorage interface {
	SummaryBySlotAndGroup(ctx context.Context, slotID int64, groupID int64) ([]entities.StatisticsSummary, error)
	Add(ctx context.Context, item *entities.Statistics) error
}
