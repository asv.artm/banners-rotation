package interfaces

import (
	"context"

	"gitlab.com/asv.artm/banners-rotation/internal/domain/entities"
)

type RotationStorage interface {
	List(ctx context.Context) ([]entities.Rotation, error)
	ListBySlot(ctx context.Context, slotID int64) ([]entities.Rotation, error)
	Get(ctx context.Context, bannerID int64, slotID int64) (*entities.Rotation, error)
	Add(ctx context.Context, item *entities.Rotation) error
	Update(ctx context.Context, item *entities.Rotation) error
	Delete(ctx context.Context, bannerID int64, slotID int64) error
}
