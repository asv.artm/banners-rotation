package api

import (
	"context"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"gitlab.com/asv.artm/banners-rotation/internal/configer"
	"gitlab.com/asv.artm/banners-rotation/internal/domain"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/enums"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/interfaces"
	"gitlab.com/asv.artm/banners-rotation/internal/domain/storages"
	"gitlab.com/asv.artm/banners-rotation/internal/logger"
	proto "gitlab.com/asv.artm/banners-rotation/pkg/banners-api"
	rabbitmq "gitlab.com/asv.artm/banners-rotation/pkg/rabbitmq-producer"
)

var logr *logrus.Logger

type RotationServer struct {
	rotator          interfaces.Rotator
	statisticsSender interfaces.StatisticsSender
}

func (s RotationServer) AddBanner(ctx context.Context, request *proto.AddBannerRequest) (*proto.AddBannerResponse, error) {
	logr.Info("received add request")
	response := &proto.AddBannerResponse{}
	err := s.rotator.Add(ctx, request.BannerId, request.SlotId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on add: %v", err))
	}

	response.Success = true
	return response, nil
}

func (s RotationServer) DeleteBanner(ctx context.Context, request *proto.DeleteBannerRequest) (*proto.DeleteBannerResponse, error) {
	logr.Info("received delete request")
	response := &proto.DeleteBannerResponse{}
	err := s.rotator.Delete(ctx, request.BannerId, request.SlotId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on delete: %v", err))
	}

	response.Success = true
	return response, nil
}

func (s RotationServer) ClickBanner(ctx context.Context, request *proto.ClickBannerRequest) (*proto.ClickBannerResponse, error) {
	logr.Info("received click request")
	response := &proto.ClickBannerResponse{}
	err := s.rotator.Click(ctx, request.BannerId, request.SlotId, request.GroupId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on click: %v", err))
	}

	err = s.statisticsSender.Send(enums.Click, request.BannerId, request.SlotId, request.GroupId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on send click statistic: %v", err))
	}

	response.Success = true
	return response, nil
}

func (s RotationServer) GetBanner(ctx context.Context, request *proto.GetBannerRequest) (*proto.GetBannerResponse, error) {
	logr.Info("received get request")
	response := &proto.GetBannerResponse{}

	bannerID, err := s.rotator.Get(ctx, request.SlotId, request.GroupId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on get banner: %v", err))
	}
	if bannerID == 0 {
		return response, status.Error(codes.Internal, "error on get banner: banner id is 0")
	}

	err = s.rotator.Buyout(ctx, bannerID, request.SlotId, request.GroupId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on click: %v", err))
	}

	err = s.statisticsSender.Send(enums.Buyout, bannerID, request.SlotId, request.GroupId)
	if err != nil {
		return response, status.Error(codes.Internal, fmt.Sprintf("error on send buyout statistic: %v", err))
	}

	response.BannerId = bannerID

	return response, nil
}

func StartGrpcServer(configPath string) error {
	log.Printf("Starting gRPC server...")
	cfg := configer.ReadConfig(configPath)
	log.Printf("config: %v", cfg)
	logr = logger.NewLogger(cfg.LogFile, cfg.LogLevel)
	addr := fmt.Sprintf("%s:%d", cfg.GrpcHost, cfg.GrpcPort)
	listen, err := net.Listen("tcp", addr)
	if err != nil {
		return fmt.Errorf("failed to listen %v", err)
	}

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)

	db, err := sqlx.Open("pgx", cfg.PostgresDsn)
	if err != nil {
		return fmt.Errorf("connection to database failed: %v", err)
	}

	rotator, err := initRotator(db)
	if err != nil {
		return fmt.Errorf("unable to create rotation service: %v", err)
	}
	log.Println("connected to database")

	ctx := context.Background()
	producer := rabbitmq.NewProducer(ctx, cfg.RabbitURL, cfg.RabbitExchange, cfg.RabbitQueue)
	err = producer.Connect()
	if err != nil {
		log.Fatalf("connection to RabbitMQ failed: %v", err)
	}
	statisticsSender := domain.NewStatisticsSender(producer)

	rotationServer := &RotationServer{rotator: rotator, statisticsSender: statisticsSender}
	proto.RegisterRotationServiceServer(grpcServer, rotationServer)
	err = grpcServer.Serve(listen)
	if err != nil {
		return fmt.Errorf("error on serve gRPC server: %v", err)
	}

	return nil
}

func initRotator(db *sqlx.DB) (interfaces.Rotator, error) {
	bannerStorage, err := storages.NewPgBannerStorage(db)
	if err != nil {
		return nil, fmt.Errorf("unable to create banner storage: %w", err)
	}

	slotStorage, err := storages.NewPgSlotStorage(db)
	if err != nil {
		return nil, fmt.Errorf("unable to create slot storage: %v", err)
	}

	groupStorage, err := storages.NewPgGroupStorage(db)
	if err != nil {
		return nil, fmt.Errorf("unable to create group storage: %v", err)
	}

	rotationStorage, err := storages.NewPgRotationStorage(db)
	if err != nil {
		return nil, fmt.Errorf("unable to create rotation storage: %v", err)
	}

	statisticStorage, err := storages.NewPgStatisticsStorage(db)
	if err != nil {
		return nil, fmt.Errorf("unable to create statistic storage: %v", err)
	}

	return domain.NewBannerRotator(bannerStorage, slotStorage, groupStorage, rotationStorage, statisticStorage, logr)
}
