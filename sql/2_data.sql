INSERT INTO public.banners (title) VALUES
('Баннер 1')
,('Баннер 2')
,('Баннер 3')
,('Баннер 4')
;

INSERT INTO public.slots (title) VALUES
('Слот 1')
,('Слот 2')
,('Слот 3')
;

INSERT INTO public.groups (title) VALUES
('Старики')
,('Дети')
;

INSERT INTO public.rotation (banner_id,slot_id,started_at) VALUES
(1,1,'2020-05-01 00:00:00.000'),
(2,1,'2020-05-01 00:00:00.000'),
(3,1,'2020-05-01 00:00:00.000'),
(1,2,'2020-05-01 00:00:00.000'),
(2,2,'2020-05-01 00:00:00.000'),
(3,2,'2020-05-01 00:00:00.000'),
(1,3,'2020-05-01 00:00:00.000'),
(2,3,'2020-05-01 00:00:00.000'),
(3,3,'2020-05-01 00:00:00.000')
;

INSERT INTO public.statistics_type (title) VALUES
('Клик')
,('Показ')
;
