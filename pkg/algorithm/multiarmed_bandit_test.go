package algorithm_test

import (
	"testing"

	"gitlab.com/asv.artm/banners-rotation/pkg/algorithm"
	"gitlab.com/asv.artm/banners-rotation/pkg/algorithm/entities"

	"github.com/sirupsen/logrus"
)

func TestGetHandle(t *testing.T) {
	logger := logrus.New()
	algorithm, err := algorithm.NewMultiarmedBandit(logger)
	if err != nil {
		t.Fatalf("error on init algorithm: %v", err)
	}

	data := []entities.AlgorithmData{
		{HandleID: 1, Count: 1000, AvgIncome: 1},
		{HandleID: 2, Count: 1, AvgIncome: 0},
		{HandleID: 3, Count: 1, AvgIncome: 0},
	}
	handleID, err := algorithm.GetHandle(data)
	if err != nil {
		t.Fatalf("bad result, %v", err)
	}
	if handleID == 0 {
		t.Fatalf("bad result, handle id can't be 0")
	}
}

func TestGetHandleInit(t *testing.T) {
	logger := logrus.New()
	algorithm, err := algorithm.NewMultiarmedBandit(logger)
	if err != nil {
		t.Fatalf("error on init algorithm: %v", err)
	}

	data := []entities.AlgorithmData{
		{HandleID: 1, Count: 0, AvgIncome: 0},
		{HandleID: 2, Count: 0, AvgIncome: 0},
		{HandleID: 3, Count: 0, AvgIncome: 0},
	}
	handleID, err := algorithm.GetHandle(data)
	if err != nil {
		t.Fatalf("bad result, %v", err)
	}
	if handleID == 0 {
		t.Fatalf("bad result, handle id can't be 0")
	}
}

func TestGetHandleNoItems(t *testing.T) {
	logger := logrus.New()
	algorithm, err := algorithm.NewMultiarmedBandit(logger)
	if err != nil {
		t.Fatalf("error on init algorithm: %v", err)
	}

	var data []entities.AlgorithmData
	handleID, err := algorithm.GetHandle(data)
	if err == nil {
		t.Fatalf("bad result, with no data shound be error")
	}
	if handleID != 0 {
		t.Fatalf("bad result, handle id should be 0")
	}
}
