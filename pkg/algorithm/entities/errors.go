package entities

type AlgorithmError struct {
	Message string
	Err     error
}

func (e AlgorithmError) Error() string { return e.Message }
