package interfaces

import "gitlab.com/asv.artm/banners-rotation/pkg/algorithm/entities"

type RotationAlgorithm interface {
	GetHandle([]entities.AlgorithmData) (int64, error)
}
