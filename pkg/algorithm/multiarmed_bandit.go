package algorithm

import (
	"fmt"

	"math"

	"github.com/sirupsen/logrus"
	"gitlab.com/asv.artm/banners-rotation/pkg/algorithm/entities"
)

type MultiarmedBandit struct {
	logger *logrus.Logger
}

func NewMultiarmedBandit(logger *logrus.Logger) (*MultiarmedBandit, error) {
	return &MultiarmedBandit{logger}, nil
}

func (mb *MultiarmedBandit) GetHandle(items []entities.AlgorithmData) (int64, error) {
	if len(items) == 0 {
		return 0, entities.AlgorithmError{Message: "wrong data"}
	}

	var countTotal int64
	for _, item := range items {
		if item.Count == 0 {
			return item.HandleID, nil
		}
		countTotal += item.Count
	}

	mb.logger.Debugf("total count: %v\n", countTotal)

	var handleID int64
	var maxResult float64
	for _, item := range items {
		result := float64(item.AvgIncome) + math.Sqrt((2*math.Log(float64(countTotal)))/float64(item.Count))
		if result > maxResult {
			maxResult = result
			handleID = item.HandleID
		}

		mb.logger.Debugf("handleId: %v, priority: %v\n", item.HandleID, result)
	}

	if math.IsInf(maxResult, 0) {
		return 0, fmt.Errorf("wrong result of algorthm, priority is infinity")
	}

	mb.logger.Debugf("handle with max priority: %v, priority: %v\n", handleID, maxResult)

	return handleID, nil
}
