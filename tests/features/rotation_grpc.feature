Feature: gRPC requests handling
	As API client of banner rotation service
	In order to understand that the service working
	I want to receive simple requests

	Scenario: Rotation gRPC API service add banner method is available
		When I send request to add banner method with bannerID=4 and slotID=1
		Then The add banner request response code should be 0 (ok)
		And The add banner request response should be with value true

	Scenario: Rotation gRPC API service delete banner method is available
		When I send request to delete banner method with bannerID=4 and slotID=1
		Then The delete banner request response code should be 0 (ok)
		And The delete banner request response should be with value true

	Scenario: Rotation gRPC API service click banner method is available
		When I send request to click banner method with bannerID=1 and slotID=1 and groupID=1
		Then The click banner request response code should be 0 (ok)
		And The click banner request response should be with value true

	Scenario: Rotation gRPC API service get banner method is available
		When I send request to get banner method with slotID=1 and groupID=2
		Then The get banner request response code should be 0 (ok)
		And The get banner request response should be not 0

	Scenario: Every banner must be shown
		When I send 1000 requests to get banner method with slotID=1 and groupID=1
		Then Each banner must be shown at least once
		And The response codes of get banner method should be 0 (ok)

	Scenario: Clicked banner should be shown more often
		When I send 1000 requests to get banner method with slotID=3 and groupID=2 and click only by one banner with bannerID=2
		Then The buyouts count of banner with bannerID=2 should be greater than the rest of the banners
		And The response codes of get banner and click banner methods should be 0 (ok)
