package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/cucumber/godog"
	"github.com/cucumber/messages-go/v10"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"

	proto "gitlab.com/asv.artm/banners-rotation/pkg/banners-api"
)

var grpcListen = os.Getenv("TESTS_GRPC_API")

func init() {
	if grpcListen == "" {
		grpcListen = "rotation_grpc_api:50052"
	}
}

type rotationGrpcTest struct {
	ctx                context.Context
	clientConn         *grpc.ClientConn
	client             proto.RotationServiceClient
	addBannerResult    bool
	addBannerCode      int
	deleteBannerResult bool
	deleteBannerCode   int
	clickBannerResult  bool
	clickBannerCode    int
	getBannerResult    int64
	getBannerCode      int
	viewed             map[int64]int
	viewError          bool
	viewedWithClicks   map[int64]int
	viewOrClickError   bool
}

func (test *rotationGrpcTest) connect(*messages.Pickle) {
	test.ctx, _ = context.WithTimeout(context.Background(), 5*time.Minute)
	cc, err := grpc.Dial(grpcListen, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	test.client = proto.NewRotationServiceClient(cc)
	test.clientConn = cc
}

func (test *rotationGrpcTest) close(*messages.Pickle, error) {
	err := test.clientConn.Close()
	if err != nil {
		fmt.Errorf("error on close connection: %v", err)
	}
}

func getStatusCode(err error) (code int) {
	statusErr, ok := status.FromError(err)
	if ok {
		code = int(statusErr.Code())
	} else {
		code = -1
	}
	return code
}

func (test *rotationGrpcTest) iSendRequestToAddBannerMethod(bannerID, slotID int64) error {
	request := &proto.AddBannerRequest{BannerId: bannerID, SlotId: slotID}
	response, err := test.client.AddBanner(test.ctx, request)
	if err != nil {
		test.addBannerCode = getStatusCode(err)
		return fmt.Errorf("error on add banner to rotation: %v", err)
	}

	test.addBannerResult = response.Success

	return nil
}

func (test *rotationGrpcTest) theAddBannerRequestResponseCodeShouldBeOk(code int) error {
	if test.addBannerCode != code {
		return fmt.Errorf("unexpected status code: %d != %d", test.addBannerCode, code)
	}
	return nil
}

func (test *rotationGrpcTest) theAddBannerRequestResponseShouldBeWithValueTrue() error {
	if !test.addBannerResult {
		return fmt.Errorf("banner not added to rotation")
	}
	return nil
}

func (test *rotationGrpcTest) iSendRequestToDeleteBannerMethod(bannerID, slotID int64) error {
	request := &proto.DeleteBannerRequest{BannerId: bannerID, SlotId: slotID}
	response, err := test.client.DeleteBanner(test.ctx, request)
	if err != nil {
		test.deleteBannerCode = getStatusCode(err)
		return fmt.Errorf("error on delete banner from rotation: %v", err)
	}

	test.deleteBannerResult = response.Success

	return nil
}

func (test *rotationGrpcTest) theDeleteBannerRequestResponseCodeShouldBeOk(code int) error {
	if test.deleteBannerCode != code {
		return fmt.Errorf("unexpected status code: %d != %d", test.deleteBannerCode, code)
	}
	return nil
}

func (test *rotationGrpcTest) theDeleteBannerRequestResponseShouldBeWithValueTrue() error {
	if !test.deleteBannerResult {
		return fmt.Errorf("banner not deleted from rotation")
	}
	return nil
}

func (test *rotationGrpcTest) iSendRequestToClickBannerMethod(bannerID, slotID, groupID int64) error {
	request := &proto.ClickBannerRequest{BannerId: bannerID, SlotId: slotID, GroupId: groupID}
	response, err := test.client.ClickBanner(test.ctx, request)
	if err != nil {
		test.deleteBannerCode = getStatusCode(err)
		return fmt.Errorf("error on click banner: %v", err)
	}

	test.clickBannerResult = response.Success

	return nil
}

func (test *rotationGrpcTest) theClickBannerRequestResponseCodeShouldBeOk(code int) error {
	if test.clickBannerCode != code {
		return fmt.Errorf("unexpected status code: %d != %d", test.clickBannerCode, code)
	}
	return nil
}

func (test *rotationGrpcTest) theClickBannerRequestResponseShouldBeWithValueTrue() error {
	if !test.clickBannerResult {
		return fmt.Errorf("error on click banner")
	}
	return nil
}

func (test *rotationGrpcTest) iSendRequestToGetBannerMethod(slotID, groupID int64) error {
	request := &proto.GetBannerRequest{SlotId: slotID, GroupId: groupID}
	response, err := test.client.GetBanner(test.ctx, request)
	if err != nil {
		test.getBannerCode = getStatusCode(err)
		return fmt.Errorf("error on get banner: %v", err)
	}

	test.getBannerResult = response.BannerId

	return nil
}

func (test *rotationGrpcTest) theGetBannerRequestResponseCodeShouldBeOk(code int) error {
	if test.getBannerCode != code {
		return fmt.Errorf("unexpected status code: %d != %d", test.getBannerCode, code)
	}
	return nil
}

func (test *rotationGrpcTest) theGetBannerRequestResponseShouldBeNot(arg1 int) error {
	if test.getBannerResult == 0 {
		return fmt.Errorf("wrong get banner result, must be non zero")
	}
	return nil
}

func (test *rotationGrpcTest) iSendRequestsToGetBannerMethod(count int, slotID, groupID int64) error {
	test.viewed = make(map[int64]int)
	for i := 0; i < count; i++ {
		request := &proto.GetBannerRequest{SlotId: slotID, GroupId: groupID}
		response, err := test.client.GetBanner(test.ctx, request)
		if err != nil {
			code := getStatusCode(err)
			if code != 0 {
				test.viewError = true
			}
			return fmt.Errorf("error on get banner: %v", err)
		}
		test.viewed[response.BannerId]++
	}

	return nil
}

func (test *rotationGrpcTest) eachBannerMustBeShownAtLeastOnce() error {
	if len(test.viewed) != 3 {
		return fmt.Errorf("not all banners were shown")
	}
	return nil
}

func (test *rotationGrpcTest) allGetBannerResponseCodesShouldBeOk(code int) error {
	if test.viewError {
		return fmt.Errorf("not all responses were with status code %d", code)
	}
	return nil
}

func (test *rotationGrpcTest) iSendRequestsToGetBannerMethodAndClickOnlyByOneBanner(count int, slotID, groupID, bannerID int64) error {
	test.viewedWithClicks = make(map[int64]int)
	for i := 0; i < count; i++ {
		getRequest := &proto.GetBannerRequest{SlotId: slotID, GroupId: groupID}
		getResponse, err := test.client.GetBanner(test.ctx, getRequest)
		if err != nil {
			code := getStatusCode(err)
			if code != 0 {
				test.viewOrClickError = true
			}
			return fmt.Errorf("error on get banner: %v", err)
		}

		test.viewedWithClicks[getResponse.BannerId]++

		if getResponse.BannerId == bannerID {
			clickRequest := &proto.ClickBannerRequest{
				BannerId: getResponse.BannerId,
				SlotId:   slotID,
				GroupId:  groupID,
			}
			clickResponse, err := test.client.ClickBanner(test.ctx, clickRequest)
			if err != nil {
				code := getStatusCode(err)
				if code != 0 {
					test.viewOrClickError = true
				}
				return fmt.Errorf("error on click banner: %v", err)
			}
			if !clickResponse.Success {
				test.viewOrClickError = true
			}
		}
	}

	return nil
}

func (test *rotationGrpcTest) buyoutsCountOfBannerShouldBeGreaterThanTheRestOfTheBanners(bannerID int64) error {
	var maxViews int
	var maxViewsBannerID int64
	for bannerID, views := range test.viewedWithClicks {
		if maxViews < views {
			maxViews = views
			maxViewsBannerID = bannerID
		}
	}

	if maxViewsBannerID != bannerID {
		return fmt.Errorf("wrong result banner, should be banner %d", bannerID)
	}

	return nil
}

func (test *rotationGrpcTest) allGetBannerAndClickBannerResponseCodesShouldBeOk(code int) error {
	if test.viewOrClickError {
		return fmt.Errorf("not all responses were with status code %d", code)
	}
	return nil
}

func FeatureContext(s *godog.Suite) {
	testGrpc := new(rotationGrpcTest)

	s.BeforeScenario(testGrpc.connect)

	s.Step(`^I send request to add banner method with bannerID=(\d+) and slotID=(\d+)$`, testGrpc.iSendRequestToAddBannerMethod)
	s.Step(`^The add banner request response code should be (\d+) \(ok\)$`, testGrpc.theAddBannerRequestResponseCodeShouldBeOk)
	s.Step(`^The add banner request response should be with value true$`, testGrpc.theAddBannerRequestResponseShouldBeWithValueTrue)

	s.Step(`^I send request to click banner method with bannerID=(\d+) and slotID=(\d+) and groupID=(\d+)$`, testGrpc.iSendRequestToClickBannerMethod)
	s.Step(`^The click banner request response code should be (\d+) \(ok\)$`, testGrpc.theClickBannerRequestResponseCodeShouldBeOk)
	s.Step(`^The click banner request response should be with value true$`, testGrpc.theClickBannerRequestResponseShouldBeWithValueTrue)

	s.Step(`^I send request to delete banner method with bannerID=(\d+) and slotID=(\d+)$`, testGrpc.iSendRequestToDeleteBannerMethod)
	s.Step(`^The delete banner request response code should be (\d+) \(ok\)$`, testGrpc.theDeleteBannerRequestResponseCodeShouldBeOk)
	s.Step(`^The delete banner request response should be with value true$`, testGrpc.theDeleteBannerRequestResponseShouldBeWithValueTrue)

	s.Step(`^I send request to get banner method with slotID=(\d+) and groupID=(\d+)$`, testGrpc.iSendRequestToGetBannerMethod)
	s.Step(`^The get banner request response code should be (\d+) \(ok\)$`, testGrpc.theGetBannerRequestResponseCodeShouldBeOk)
	s.Step(`^The get banner request response should be not (\d+)$`, testGrpc.theGetBannerRequestResponseShouldBeNot)

	s.Step(`^I send (\d+) requests to get banner method with slotID=(\d+) and groupID=(\d+)$`, testGrpc.iSendRequestsToGetBannerMethod)
	s.Step(`^Each banner must be shown at least once$`, testGrpc.eachBannerMustBeShownAtLeastOnce)
	s.Step(`^The response codes of get banner method should be (\d+) \(ok\)$`, testGrpc.allGetBannerResponseCodesShouldBeOk)

	s.Step(`^I send (\d+) requests to get banner method with slotID=(\d+) and groupID=(\d+) and click only by one banner with bannerID=(\d+)$`, testGrpc.iSendRequestsToGetBannerMethodAndClickOnlyByOneBanner)
	s.Step(`^The buyouts count of banner with bannerID=(\d+) should be greater than the rest of the banners$`, testGrpc.buyoutsCountOfBannerShouldBeGreaterThanTheRestOfTheBanners)
	s.Step(`^The response codes of get banner and click banner methods should be (\d+) \(ok\)$`, testGrpc.allGetBannerAndClickBannerResponseCodesShouldBeOk)

	s.AfterScenario(testGrpc.close)
}
