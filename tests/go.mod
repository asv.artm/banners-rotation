module gitlab.com/asv.artm/banners-rotation/integration-tests

go 1.14

require (
	github.com/cucumber/godog v0.9.0
	github.com/cucumber/messages-go/v10 v10.0.3
	gitlab.com/asv.artm/banners-rotation v0.0.0-20200519204220-1338202976db
	google.golang.org/grpc v1.29.1
)
