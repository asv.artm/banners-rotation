package main

import (
	"log"

	_ "github.com/jackc/pgx/stdlib"
	"github.com/spf13/pflag"

	"gitlab.com/asv.artm/banners-rotation/internal/api"
)

var configPath string

func init() {
	pflag.StringVarP(&configPath, "config", "c", "configs/config.json", "Config file path")
	pflag.Parse()
}
func main() {
	err := api.StartGrpcServer(configPath)
	if err != nil {
		log.Fatalf("gRPC server error: %v", err)
	}
}
