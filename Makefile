PATH_API=./cmd/rotation_api
PATH_COMPOSE=./build/docker-compose/docker-compose.yml
PATH_COMPOSE_TEST=./build/docker-compose/docker-compose.test.yml

.PHONY: build
build:
	go build $(PATH_API)

.PHONY: lint
lint:
	golangci-lint run
	cd ./pkg/algorithm && golangci-lint run --config=../../.golangci.yml

.PHONY: unit-tests
unit-tests:
	cd ./pkg/algorithm && go test -race -count 100 -v 2>&1 | go-junit-report > ../report.xml

.PHONY: install-deps
install-deps:
	GO11MODULE=off go get -u github.com/jstemmer/go-junit-report

.PHONY: run-api
run-api:
	go run $(PATH_API) --config=configs/config.json

.PHONY: test
test:
	set -e ;\
	docker-compose -f $(PATH_COMPOSE_TEST) up --build -d ;\
	test_status_code=0 ;\
	docker-compose -f $(PATH_COMPOSE_TEST) run integration_tests go test || test_status_code=$$? ;\
	docker-compose -f $(PATH_COMPOSE_TEST) down ;\
	exit $$test_status_code ;\

.PHONY: run
run:
	docker-compose -f $(PATH_COMPOSE) up -d --build

.PHONY: stop
stop:
	docker-compose -f $(PATH_COMPOSE) down

.PHONY: restart
restart: stop run

.PHONY: generate-proto
generate-proto:
	protoc ./api/rotation.proto --go_out=plugins=grpc:./pkg/banners-api